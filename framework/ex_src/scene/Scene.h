#pragma once

#include "core/open_gl.h"
#include "core/camera/ArcballCamera.h"
#include "core/camera/FreeCamera.h"
#include <SyncedData.h>
#include <core/gfx/FullscreenQuad.h>
#include <core/gfx/mesh/Mesh.h>
#include <core/gfx/mesh/MeshRenderable.h>
#include <glm/glm.hpp>

namespace viscom {
    class GPUProgram;
    class FrameBuffer;
    class ApplicationNodeBase;
} // namespace viscom

#define FREE_FLIGHT_CAMERA 0
#define ARCBALL_CAMERA 1

namespace cg1 {

#define CAMERA_MODE FREE_FLIGHT_CAMERA

#if CAMERA_MODE == ARCBALL_CAMERA
    using ICGCamera = viscom::ArcballCamera;
#define CURSOR_MODE GLFW_CURSOR_NORMAL
#elif CAMERA_MODE == FREE_FLIGHT_CAMERA
    using CG1Camera = viscom::FreeCamera;
#define CURSOR_MODE GLFW_CURSOR_DISABLED
#endif


    /** The vertex object used in these examples. */
    struct Vertex
    {
        /** Standard constructor. */
        Vertex() : position{}, color{}, normal{} {};
        /** Constructor to initialize the vertex. */
        Vertex(const glm::vec4& pos, const glm::vec4& col, const glm::vec3& norm)
            : position{ pos }, color{ col }, normal{ norm } {};
        /** The vertex position. */
        glm::vec4 position;
        /** The vertex color. */
        glm::vec4 color;
        /** The vertex normal. */
        glm::vec3 normal;
    };

    class Scene
    {
    public:
        Scene(viscom::ApplicationNodeBase* app, const viscom::FWConfiguration& config);
        ~Scene();

        void RenderScene(viscom::FrameBuffer& fbo);
        void RenderCornellBox(const viscom::FrameBuffer& fbo);
        void Render3dObject(const viscom::FrameBuffer& fbo);
        void RenderGUI(const viscom::FrameBuffer& fbo);
        void RenderBlur(const viscom::FrameBuffer& vBlurFBO, const viscom::FrameBuffer& hBlurFBO, GLuint texture);
        void UpdateScene(double currentTime, double elapsedTime, viscom::ApplicationNodeBase* app) noexcept;
        bool KeyboardCallback(int /* key */, int /* scancode */, int /*action */, int /* mods */) { return false; }
        bool CharCallback(unsigned int /* character */, int /* mods */) { return false; }
        bool MouseScrollCallback(double /* xoffset */, double yoffset, viscom::ApplicationNodeBase* app)
        {
            return camera_.HandleMouse(-1, 0, static_cast<float>(yoffset), app);
        };
        bool MouseButtonCallback(int button, int action, viscom::ApplicationNodeBase* app)
        {
            return camera_.HandleMouse(button, action, 0.0f, app);
        };
        bool MousePosCallback(double /* x */, double /* y */, viscom::ApplicationNodeBase* app)
        {
            return camera_.HandleMouse(-1, 0, 0.0f, app);
        };

        SyncedData& GetSyncedData() noexcept { return syncedData_; }

    private:

        static glm::vec3 GenerateNormal(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c);

        /** Holds the camera object. */
        cg1::CG1Camera camera_;
        /** Holds the scenes GPU program. */
        std::shared_ptr<viscom::GPUProgram> programTexturedMesh_;
        std::shared_ptr<viscom::GPUProgram> programShadowMap_;
        /** Holds the OpenGL vertex buffer id. */
        GLuint vertexBuffer_;
        /** Holds the OpenGL vertex array object id. */
        GLuint vertexArrayObject_;

		std::shared_ptr<viscom::MeshRenderable> meshRenderable_;

		 /** Holds the road mesh. */
        std::shared_ptr<viscom::Mesh> skyMesh_;
        /** Holds the road mesh renderable. */
        std::shared_ptr<viscom::MeshRenderable> skyMeshRenderable_;

        /** Holds the road mesh. */
        std::shared_ptr<viscom::Mesh> roadMesh_;
        /** Holds the road mesh renderable. */
        std::shared_ptr<viscom::MeshRenderable> roadMeshRenderable_;

		/** Holds the aircraft mesh. */
        std::shared_ptr<viscom::Mesh> aircraftMesh_;
        /** Holds the aircraft mesh renderable. */
        std::shared_ptr<viscom::MeshRenderable> aircraftMeshRenderable_;

		/** Holds the wall mesh. */
        std::shared_ptr<viscom::Mesh> wallMesh_;
        /** Holds the wall mesh renderable. */
        std::shared_ptr<viscom::MeshRenderable> wallMeshRenderable_;

		/** Holds the wall mesh. */
        std::shared_ptr<viscom::Mesh> tropicTerrainMesh_;
        /** Holds the wall mesh renderable. */
        std::shared_ptr<viscom::MeshRenderable> tropicTerrainMeshRenderable_;

        std::shared_ptr<viscom::FrameBuffer> shadowbuffer_;

        /** Holds uniform name for the model matrix. */
        GLint matModelUniformLocation_;
        /** Holds uniform name for the normal matrix. */
        GLint matNormalUniformLocation_;
        /** Holds uniform name for the model-view-projection matrix. */
        GLint matMVPUniformLocation_;
        /** Holds uniform name for the model-view-projection matrix. */
        GLint matMVPUniformLocationTexturedMesh_;

        GLint vecLightPosition_;

        GLint vecCameraPosition_;

		GLint l_d;
        GLint l_a;
        GLint l_s;

        /** Holds the model matrix. */
        glm::mat4 modelMatrix_;
        /** Holds the normal matrix. */
        glm::mat4 normalMatrix_;
        /** Holds the MVP matrix. */
        glm::mat4 MVPMatrix_;

		/** Hold the terrain model matrices */
        glm::mat4 tropicTerrainModelMatrix_;
        /** Hold the sky model matrices */
        glm::mat4 skynModelMatrix_;

        /** Hold the aircraft model matrices. */
		glm::mat4 aircraft1ModelMatrix_;
        glm::mat4 aircraft2ModelMatrix_;
        /** Holds the road model matrix */
		glm::mat4 roadModelMatrix_;
        /** Hold the wall model matrices */
        glm::mat4 leftWallModelMatrix_;
        glm::mat4 rightWallModelMatrix_;

        glm::vec3 LightPos_;
        glm::vec3 CameraPos_;

        viscom::ApplicationNodeBase* app_;

        std::vector<viscom::FrameBuffer> preFBO0_;
        std::vector<viscom::FrameBuffer> preFBO1_;

        std::unique_ptr<viscom::FullscreenQuad> vBlurQuad_;
        std::unique_ptr<viscom::FullscreenQuad> hBlurQuad_;

        SyncedData syncedData_;
    };
} // namespace cg1
