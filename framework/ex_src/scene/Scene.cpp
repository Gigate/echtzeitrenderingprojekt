
#include "Scene.h"
#include "app/ApplicationNodeImplementation.h"
#include "core/open_gl.h"
#include <app/Vertices.h>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>
#include <iostream>

namespace cg1 {
    /**
     *  Constructor.
     */
    Scene::Scene(viscom::ApplicationNodeBase* app, const viscom::FWConfiguration&)
        : camera_{glm::vec3(0.0f, 5.0f, 0.0f), *app->GetCamera()},
          programTexturedMesh_{app->GetGPUProgramManager().GetResource(
              "TexturedMeshProgram", std::vector<std::string>{"mesh.vert", "mesh.frag"})},
          programShadowMap_{app->GetGPUProgramManager().GetResource(
          "TexturedMeshProgram", std::vector<std::string>{"shadowmap_vertex.glsl", "shadowmap_fragment.glsl"})},
          vertexBuffer_{0},
          vertexArrayObject_{0},
          roadMesh_{app->GetMeshManager().GetResource("/models/road/untitled.obj")},
          aircraftMesh_{app->GetMeshManager().GetResource("/models/E-45-Aircraft/E 45 Aircraft_obj.obj")},
          wallMesh_{app->GetMeshManager().GetResource("/models/wall/wall.obj")},
          tropicTerrainMesh_{
              app->GetMeshManager().GetResource("/models/Small Tropical Island/Small Tropical Island.obj")},
          matModelUniformLocation_{-1},
          matNormalUniformLocation_{-1},
          matMVPUniformLocation_{-1},
          matMVPUniformLocationTexturedMesh_{-1},
          modelMatrix_{1.0f},
          normalMatrix_{1.0f},
          MVPMatrix_{1.0f},
          tropicTerrainModelMatrix_{1.0f},
          aircraft1ModelMatrix_{1.0},
          aircraft2ModelMatrix_{1.0},
          roadModelMatrix_{1.0f},
          leftWallModelMatrix_{1.0f},
          rightWallModelMatrix_{1.0f},
          app_{app}
    {
        // init model matrices
        tropicTerrainModelMatrix_ =
            glm::rotate(tropicTerrainModelMatrix_, glm::half_pi<float>(), glm::vec3(0.0f, 1.0f, 0.0f));
        tropicTerrainModelMatrix_ = glm::translate(tropicTerrainModelMatrix_, glm::vec3(35.0f, 1.0f, 0.0f));
        tropicTerrainModelMatrix_ = glm::scale(tropicTerrainModelMatrix_, glm::vec3(0.1f));

        aircraft1ModelMatrix_ = glm::translate(aircraft1ModelMatrix_, glm::vec3(30.0f, 3.5f, 0.0f));
        aircraft1ModelMatrix_ = glm::scale(aircraft1ModelMatrix_, glm::vec3(0.5f));
        aircraft2ModelMatrix_ = glm::rotate(aircraft2ModelMatrix_, glm::pi<float>(), glm::vec3(0, 1, 0));
        aircraft2ModelMatrix_ = glm::translate(aircraft2ModelMatrix_, glm::vec3(30.0f, 3.5f, 0.0f));
        aircraft2ModelMatrix_ = glm::scale(aircraft2ModelMatrix_, glm::vec3(0.5f));

        roadModelMatrix_ = glm::scale(roadModelMatrix_, glm::vec3(2.0f));

        leftWallModelMatrix_ = glm::translate(leftWallModelMatrix_, glm::vec3(-30, 3.0f, -5.5f));
        rightWallModelMatrix_ = glm::rotate(rightWallModelMatrix_, glm::pi<float>(), glm::vec3(0, 1, 0));
        rightWallModelMatrix_ = glm::translate(rightWallModelMatrix_, glm::vec3(-30, 2.5f, -3.0f));

        // create a renderable for a mesh using a specified program.
        // NOTE: the program (vert and fragment shaders) as well as the mesh need to have the same vertex type (e.g. SimpleMeshVertex)
        // e.g. SimpleMeshVertex = (Position, Normal, TexCoord)
        roadMeshRenderable_ =
            viscom::MeshRenderable::create<SimpleMeshVertex>(roadMesh_.get(), programTexturedMesh_.get());
        aircraftMeshRenderable_ =
            viscom::MeshRenderable::create<SimpleMeshVertex>(aircraftMesh_.get(), programTexturedMesh_.get());
        wallMeshRenderable_ =
            viscom::MeshRenderable::create<SimpleMeshVertex>(wallMesh_.get(), programTexturedMesh_.get());
        tropicTerrainMeshRenderable_ =
            viscom::MeshRenderable::create<SimpleMeshVertex>(tropicTerrainMesh_.get(), programTexturedMesh_.get());

       //TODO: Renderable for shadowmap program

        // get the location for the MVP matrix which is used to draw the sphere and cube.
        matMVPUniformLocationTexturedMesh_ = glGetUniformLocation(programTexturedMesh_->getProgramId(), "vpMatrix");
        vecLightPosition_ = glGetUniformLocation(programTexturedMesh_->getProgramId(), "lightPos");
        vecCameraPosition_ = glGetUniformLocation(programTexturedMesh_->getProgramId(), "cameraPos");
        // get locations for light source
        l_d = glGetUniformLocation(programTexturedMesh_->getProgramId(), "L_d");
        l_a = glGetUniformLocation(programTexturedMesh_->getProgramId(), "L_a");
        l_s = glGetUniformLocation(programTexturedMesh_->getProgramId(), "L_s");

        // create a description to specify the type of the offscreen buffers.
        std::vector<viscom::FrameBufferTextureDescriptor> fboDesc{viscom::FrameBufferTextureDescriptor(GL_RGBA8)};
        std::vector<viscom::RenderBufferDescriptor> rboDesc{viscom::RenderBufferDescriptor(GL_DEPTH24_STENCIL8)};

        viscom::FrameBufferDescriptor desc = viscom::FrameBufferDescriptor(fboDesc,rboDesc);
        shadowbuffer_ = std::shared_ptr<viscom::FrameBuffer>(new viscom::FrameBuffer(1024,1024, desc));

        LightPos_ = glm::vec3(0, 5, 0);

        float aspect = (float)1024/(float)1024;
        float near = 1.0f;
        float far = 25.0f;
        glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, near, far);

        std::vector<glm::mat4> shadowTransforms;
        shadowTransforms.push_back(shadowProj *
                         glm::lookAt(LightPos_, LightPos_ + glm::vec3( 1.0, 0.0, 0.0), glm::vec3(0.0,-1.0, 0.0)));
        shadowTransforms.push_back(shadowProj *
                         glm::lookAt(LightPos_, LightPos_ + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0,-1.0, 0.0)));
        shadowTransforms.push_back(shadowProj *
                         glm::lookAt(LightPos_, LightPos_ + glm::vec3( 0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
        shadowTransforms.push_back(shadowProj *
                         glm::lookAt(LightPos_, LightPos_ + glm::vec3( 0.0,-1.0, 0.0), glm::vec3(0.0, 0.0,-1.0)));
        shadowTransforms.push_back(shadowProj *
                         glm::lookAt(LightPos_, LightPos_ + glm::vec3( 0.0, 0.0, 1.0), glm::vec3(0.0,-1.0, 0.0)));
        shadowTransforms.push_back(shadowProj *
                                   glm::lookAt(LightPos_, LightPos_ + glm::vec3( 0.0, 0.0,-1.0), glm::vec3(0.0,-1.0, 0.0)));

    }

    /**
     *  Destructor.
     */
    Scene::~Scene()
    {
        if (vertexArrayObject_ != 0) glDeleteVertexArrays(1, &vertexArrayObject_);
        if (vertexBuffer_ != 0) glDeleteBuffers(1, &vertexBuffer_);
    }

    glm::vec3 Scene::GenerateNormal(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c)
    {
        glm::vec3 dir1 = glm::vec3(b) - glm::vec3(a);
        glm::vec3 dir2 = glm::vec3(c) - glm::vec3(a);
        glm::vec3 cp = glm::cross(dir1, dir2);
        return glm::normalize(cp);
    }


    /**
     *  Updates the current scene.
     *  @param currentTime the current application time.
     *  @param elapsedTime the time elapsed during the last frame.
     *  @param app the ApplicationNodeBase needed to update the Camera
     */
    void Scene::UpdateScene(double /* currentTime */, double elapsedTime, viscom::ApplicationNodeBase* app) noexcept
    {
        camera_.UpdateCamera(elapsedTime, app);
        CameraPos_ = camera_.GetPosition();
    }

    /**
     *  Renders the scene.
     *  @param fbo the FrameBuffer used to render the Scene into
     */
    void Scene::RenderScene(viscom::FrameBuffer& fbo)
    {
    //

        glUseProgram(programTexturedMesh_->getProgramId());
        glUniform3fv(l_d, 1, glm::value_ptr(glm::vec3(1.0f, 1.0f, 1.0f)));
        glUniform3fv(l_a, 1, glm::value_ptr(glm::vec3(1.0f, 1.0f, 1.0f)));
        glUniform3fv(l_s, 1, glm::value_ptr(glm::vec3(1.0f, 1.0f, 1.0f)));
        glUniform3fv(vecLightPosition_, 1, reinterpret_cast<GLfloat*>(&LightPos_));
        glUniform3fv(vecCameraPosition_, 1, reinterpret_cast<GLfloat*>(&CameraPos_));

        modelMatrix_ = tropicTerrainModelMatrix_;
        meshRenderable_ = tropicTerrainMeshRenderable_;
        Render3dObject(fbo);

        modelMatrix_ = roadModelMatrix_;
        meshRenderable_ = roadMeshRenderable_;
        Render3dObject(fbo);

        meshRenderable_ = wallMeshRenderable_;
        for (int i = 0; i < 30; i++) {
            modelMatrix_ = glm::translate(leftWallModelMatrix_, glm::vec3(i * 2, 0, 0));
            Render3dObject(fbo);
        }
        for (int i = 0; i < 60; i++) {
            modelMatrix_ = glm::translate(rightWallModelMatrix_, glm::vec3(i, 0, 0));
            modelMatrix_ = glm::scale(modelMatrix_, glm::vec3(0.5f));
            Render3dObject(fbo);
        }

        modelMatrix_ = aircraft1ModelMatrix_;
        meshRenderable_ = aircraftMeshRenderable_;
        Render3dObject(fbo);

        modelMatrix_ = aircraft2ModelMatrix_;
        Render3dObject(fbo);
    }

    /**
    * Renders a 3d Object
    * @param fbo the FrameBuffer used to render the Scene into
    */
    void Scene::Render3dObject(const viscom::FrameBuffer& fbo)
    {
        // setup the matrices
        MVPMatrix_ = camera_.GetViewProjMatrix();

        // draw into the framebuffer
        fbo.DrawToFBO([this]() {
            glUseProgram(programTexturedMesh_->getProgramId());
            // setup uniforms for the shader
            glUniformMatrix4fv(matMVPUniformLocationTexturedMesh_, 1, GL_FALSE,
                               reinterpret_cast<GLfloat*>(&MVPMatrix_));

            // draw call for sphere
            meshRenderable_->Draw(modelMatrix_);

            glUseProgram(0);
        });
    }

    /**
     *  Renders the ImGui content.
     *  @param fbo the FrameBuffer used to render the GUI into
     */
    void Scene::RenderGUI(const viscom::FrameBuffer& fbo)
    {
        fbo.DrawToFBO([this]() {
            ImGui::SetNextWindowPos(ImVec2(10, 10), ImGuiSetCond_FirstUseEver);
            ImGui::SetNextWindowSize(ImVec2(100, 100), ImGuiSetCond_FirstUseEver);
            ImGui::Begin("Render Parameters");
            ImGui::Checkbox("Blurring activated? ", reinterpret_cast<bool*>(&syncedData_.blurActivated_));
            ImGui::End();
        });
    }
} // namespace cg1
