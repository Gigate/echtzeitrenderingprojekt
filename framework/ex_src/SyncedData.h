#pragma once

/**
 * This struct holds all the data that needs to be synced across all slaves.
 * Only primitive types can be put here.
 * All the variables need to be initialized.
 */
struct SyncedData
{
    bool blurActivated_ = true;
};
