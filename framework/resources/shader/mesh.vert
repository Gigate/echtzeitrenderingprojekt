#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoords;

uniform mat4 modelMatrix;
uniform mat3 normalMatrix;
uniform mat4 vpMatrix;

uniform vec3 lightPos;
uniform vec3 cameraPos;

out vec3 fragNormal;
out vec2 fragTexCoord;
out vec3 worldPos;
out vec3 toLight;
out vec3 toCamera;

void main()
{
    fragTexCoord = texCoords;
    fragNormal = normalMatrix * normal;

    vec4 worldPosition = modelMatrix * position;
    worldPos = worldPosition.xyz;
    toLight = lightPos - worldPosition.xyz;
    toCamera = cameraPos - worldPosition.xyz;

    gl_Position = vpMatrix * worldPosition;
}
