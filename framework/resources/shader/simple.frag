#version 330 core

in vec4 fragColor;
in vec3 fragNormal;

out vec4 outputColor;

void main()
{
    const vec3 lightDir = normalize(vec3(0.5f, 1.0f, -0.5f));
    const float lightIntensity = 1.5f;

    const float ambientCoefficent = 0.3f;
    float diffuseCoefficent = clamp(dot(normalize(fragNormal), lightDir), 0.0f, 1.0f);

    vec4 ambientColor = ambientCoefficent * fragColor;
    vec4 diffuseColor = diffuseCoefficent * fragColor * lightIntensity;

    outputColor = ambientColor + diffuseColor;
    outputColor.a = 1.0f;
}
