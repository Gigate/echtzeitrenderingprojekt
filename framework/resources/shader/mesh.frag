#version 330 core

in vec3 fragNormal;
in vec2 fragTexCoord;

in vec3 worldPos;
in vec3 toLight;
in vec3 toCamera;


uniform vec3 L_d;  // diffuse light intensity
uniform vec3 L_a;     // ambient light intensity
uniform vec3 L_s; // specular light intensity

uniform sampler2D diffuseTexture;
uniform sampler2D bumpTexture;

out vec4 outputColor;

const float bumpscale = 1.0f;
const vec3 k_d = vec3(0.5f, 0.5f, 0.5f);
const vec3 k_a = vec3(0.1f, 0.1f, 0.1f);
const vec3 k_s = vec3(0.5f, 0.5f, 0.5f);
const float p = 10; 

vec2 dHdxy_fwd( sampler2D bumpMap, vec2 uv, float bumpScale ){
        vec2 dSTdx    = dFdx( uv );
        vec2 dSTdy    = dFdy( uv );
        float Hll    = bumpScale * texture( bumpMap, uv ).x;
        float dBx    = bumpScale * texture( bumpMap, uv + dSTdx ).x - Hll;
        float dBy    = bumpScale * texture( bumpMap, uv + dSTdy ).x - Hll;
        return vec2( dBx, dBy );
    }

vec3 perturbNormalArb( vec3 surf_pos, vec3 surf_norm, vec2 dHdxy ){
        // Workaround for Adreno 3XX dFd*( vec3 ) bug. See #9988
        vec3 vSigmaX = vec3( dFdx( surf_pos.x ), dFdx( surf_pos.y ), dFdx( surf_pos.z ) );
        vec3 vSigmaY = vec3( dFdy( surf_pos.x ), dFdy( surf_pos.y ), dFdy( surf_pos.z ) );
        vec3 vN = surf_norm;        // normalized
        vec3 R1 = cross( vSigmaY, vN );
        vec3 R2 = cross( vN, vSigmaX );

        float fDet = dot( vSigmaX, R1 );
        fDet *= ( float( gl_FrontFacing ) * 2.0 - 1.0 );

        vec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );
        return normalize( abs( fDet ) * surf_norm - vGrad );
    }

void main()
{
    vec3 baseColor = texture( diffuseTexture, fragTexCoord).rgb;
    vec3 normal = normalize(fragNormal);
    vec3 fragToLight = normalize(toLight);
    vec3 fragToCamera = normalize(toCamera);

    //Calc New Fragment Normal based on Bump Map.
    vec2 dHdxy        = dHdxy_fwd(bumpTexture, fragTexCoord, bumpscale);
    vec3 bumpNorm     = perturbNormalArb(worldPos, normal, dHdxy); //values between -1 > 1
    vec3 I_diffuse =  baseColor * L_d * max(0.0, dot(bumpNorm, fragToLight));
	vec3 I_ambient =  baseColor * L_a;
	vec3 h = normalize(fragToLight + fragToCamera);
	vec3 I_specular = baseColor * L_s * pow(max(0.0, dot(h, bumpNorm)), p);

	vec3 I = I_diffuse + I_ambient + I_specular;
    outputColor = vec4(I, 1.0f);
}
