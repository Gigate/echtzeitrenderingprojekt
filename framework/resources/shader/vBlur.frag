#version 330

uniform sampler2D blurTexture;

in vec2 texCoord;
out vec4 outColor;

// blur algorithm from:
// https://software.intel.com/en-us/blogs/2014/07/15/an-investigation-of-fast-real-time-gpu-based-image-blur-algorithms
const float weights[2] = float[] (0.44908, 0.05092);
const float offsets[2] = float[] (0.53805, 2.06278);

vec3 gaussianBlur(vec2 p, vec2 offset)
{
    vec3 result = vec3(0.0f);

    for (int i = 0; i < 2; ++i) {
        vec2 texOffset = offsets[i] * offset;
        vec3 color = texture(blurTexture, p + texOffset).xyz +
            texture(blurTexture, p - texOffset).xyz;
        result += weights[i] * color;
    }

    return result;
}

void main() {
    vec2 offset = vec2(0, 1.0/textureSize(blurTexture, 0).y);
    vec3 blurColor = gaussianBlur(texCoord, offset);
    outColor = vec4(blurColor, 1.0);
}
