#version 330 core

in vec4 position;
in vec4 color;
in vec3 normal;

uniform mat4 modelMatrix;
uniform mat3 normalMatrix;
uniform mat4 mvpMatrix;

out vec4 fragColor;
out vec3 fragNormal;

void main()
{
    fragColor = color;
    fragNormal = normalMatrix * normal;
    gl_Position = mvpMatrix * position;
    
}
