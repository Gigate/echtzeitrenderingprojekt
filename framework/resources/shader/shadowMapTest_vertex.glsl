#version 330 core

layout(location = 0) in vec4 position;
layout(location = 2) in vec2 texCoords;

uniform mat4 mvpMatrix;

out vec2 fragTexCoord;

void main()
{
    fragTexCoord = texCoords;
	gl_Position = mvpMatrix * position;
}
