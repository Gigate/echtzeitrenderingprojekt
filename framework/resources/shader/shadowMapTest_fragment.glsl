#version 330 core

uniform sampler2D shadowMapTexture;

in vec2 fragTexCoord;

out vec4 outputColor;

void main()
{
 outputColor = texture(shadowMapTexture, fragTexCoord);   
}
